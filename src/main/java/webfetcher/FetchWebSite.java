package webfetcher;

import java.net.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.remote.*;  

public class FetchWebSite {   
  public static void main(String[] args) throws MalformedURLException {          
    WebDriver driver = new RemoteWebDriver(         
        new URL("http://" + args[0] + ":" + args[1]),
        new ChromeOptions());     

    driver.get("https://timegov.boulder.nist.gov/");

    // Below is the xpath element as found in Google Chrome DevTools - "copy full xpath"
    // /html/body/div[1]/div[2]/div[1]/div[3]/div/div/div[3]/div[2]/time
    WebElement textElement = driver.findElement(By.xpath("/html/body/div[1]/div[2]/div[1]/div[3]/div/div/div[3]/div[2]/time"));
    String text = textElement.getText();
    System.out.println("\n\n\n\nEXTRACTED INFO FROM THE SITE: Mountain Time: " + text + "\n\n\n\n");

    driver.quit();   
  }
}